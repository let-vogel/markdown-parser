defmodule Parser do
  def main do
    get_file()
    |> get_paragraphs
    |> split_line
    |> get_marks
    |> save_html
  end

  def save_html(text) do
    File.write("test.html", text)
  end

  def get_marks(text) do
    Enum.map(text, fn paragraph -> 
      [first | rest] = paragraph

      rest = format_paragraph(rest)

      case first do
        "#" -> "<h1> #{rest} </h1>"
        "##" -> "<h2> #{rest} </h2>"
        "###" -> "<h3> #{rest} </h3>"
        "####" -> "<h4> #{rest} </h4>"
        "#####" -> "<h5> #{rest} </h5>"
        "######" -> "<h6> #{rest} </h6>"
        _ -> "<p> #{first} #{rest} </p>"
      end
    end)
  end

  def split_line(text) do
    Enum.map(text, fn paragraph -> String.split(paragraph, [" "]) end)
  end

  def get_paragraphs(text) do
    String.split(text, ["\n\n"])
  end

  def get_file do
    path = IO.gets "Provide a path for the markdown file: "
    path = String.trim(path, "\n")

    case File.read(path) do
      {:ok, body} ->
        body
      {:error, reason} ->
        IO.puts reason
    end
  end

  def format_paragraph(paragraph) do
    Enum.join(paragraph, " ") |> String.replace("\n", "<br />")
  end
end
